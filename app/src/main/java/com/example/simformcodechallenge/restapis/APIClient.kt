package com.example.simformcodechallenge.restapis

import com.example.simformcodechallenge.model.Users
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

object APIClient
{
    private val apiInterface: APIInterface

    init
    {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        apiInterface = Retrofit.Builder()
            .baseUrl(Constants.BASE_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(APIInterface::class.java)
    }

   suspend fun getUsers(): Response<Users?>?
    {
        return apiInterface.getUsers()
    }
}

interface APIInterface {
    @Headers("Accept:application/json")
    @GET(Constants.API)
    suspend fun getUsers(): Response<Users?>?

}