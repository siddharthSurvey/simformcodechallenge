package com.example.simformcodechallenge.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.example.codechallengeweatherapi.DB.UserRepository
import com.example.codechallengeweatherapi.DB.UserRoomDatabase
import com.example.simformcodechallenge.db.UsersEntity
import com.example.simformcodechallenge.model.Users
import com.example.simformcodechallenge.restapis.APIClient
import com.example.simformcodechallenge.restapis.Constants
import com.example.simformcodechallenge.restapis.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserListViewModel(application: Application) : AndroidViewModel(application) {

    private var userRepository: UserRepository
    lateinit var allUsers: LiveData<List<UsersEntity>>
    lateinit var remoteUserList : Users
    var userFailResponse =  MutableLiveData<String>()
    var isFirstTime = false

    init {
        val userDao = UserRoomDatabase.getDatabase(application).userDao()
        userRepository = UserRepository(userDao)
        allUsers = userRepository.allUsers
    }

    fun getRemoteUserList()
    {
        Coroutines.io {
            val response = APIClient.getUsers()

            if (response?.code() == Constants.SERVER_SUCCESS_CODE)
            {
                remoteUserList = response.body()!!
                fillDatabase()
            }
            else
            {
                userFailResponse.value=response?.message()
            }
        }
    }

    /*fun getAllUsers(){
        viewModelScope.launch(Dispatchers.IO) {
            allUsers = userRepository.allUsers
        }
    }*/

    private suspend fun fillDatabase(){
        viewModelScope.launch(Dispatchers.IO) {

            userRepository.deleteAll()
            remoteUserList.results?.forEach {
                var usersEntity = UsersEntity(
                    0,
                    it?.dob?.age.toString(),
                    it?.email.toString(),
                    it?.gender.toString(),
                    it?.location?.city.toString(),
                    "${it?.name?.title} ${it?.name?.first} ${it?.name?.last}",
                    it?.phone.toString(),
                    it?.picture?.thumbnail.toString()
                )
                userRepository.insert(usersEntity)
            }
            allUsers = userRepository.allUsers
        }

    }

}