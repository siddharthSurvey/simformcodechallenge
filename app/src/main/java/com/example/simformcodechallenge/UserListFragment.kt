package com.example.simformcodechallenge

import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.simformcodechallenge.adapter.UserListAdapter
import com.example.simformcodechallenge.databinding.UserListFragmentBinding
import com.example.simformcodechallenge.db.UsersEntity
import com.example.simformcodechallenge.model.Users
import com.example.simformcodechallenge.viewmodel.UserListViewModel
import kotlinx.android.synthetic.main.user_list_fragment.*

class UserListFragment : Fragment() {

    companion object {
        fun newInstance() = UserListFragment()
    }

    private lateinit var viewModel: UserListViewModel
    private lateinit var dataBinding: UserListFragmentBinding
    private lateinit var userListAdapter: UserListAdapter
    var userList = arrayListOf<UsersEntity>()

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.user_list_fragment, container, false)
        dataBinding.lifecycleOwner = this
        viewModel = ViewModelProvider(this).get(UserListViewModel::class.java)

        userListAdapter = UserListAdapter(requireContext(), userList)
        dataBinding.myAdapter = userListAdapter

        viewModel.let {
//            it.getAllUsers()
            it.allUsers.observe(viewLifecycleOwner, Observer {newList ->

                if(dataBinding.strLayout.isRefreshing)
                    dataBinding.strLayout.isRefreshing = false

                userList.clear()
                userList.addAll(newList)
                userListAdapter.notifyDataSetChanged()

            })
        }

        dataBinding.let{
            it.strLayout.setOnRefreshListener {
                if(Util.isOnline(requireContext()))
                    viewModel.getRemoteUserList()
            }
        }

        if(Util.isOnline(requireContext()))
            viewModel.getRemoteUserList()

        return dataBinding.root
    }

}