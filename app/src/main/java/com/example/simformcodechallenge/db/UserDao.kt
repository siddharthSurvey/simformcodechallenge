package com.example.codechallengeweatherapi.DB

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.example.simformcodechallenge.db.UsersEntity
import com.example.simformcodechallenge.model.Users

@Dao
interface UserDao {

    @Query("SELECT * FROM ${DBConstants.TABLE_USERS}")
    fun getAllUsers(): LiveData<List<UsersEntity>>

    @Insert
    fun insertAll(vararg users: UsersEntity)

    @Query("DELETE FROM ${DBConstants.TABLE_USERS}")
    fun deleteAll()

}