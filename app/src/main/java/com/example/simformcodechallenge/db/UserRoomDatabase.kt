package com.example.codechallengeweatherapi.DB

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.simformcodechallenge.db.UsersEntity
import com.example.simformcodechallenge.model.Users

// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = [UsersEntity::class], version = 1, exportSchema = false)
abstract class UserRoomDatabase : RoomDatabase() {

   abstract fun userDao(): UserDao

   companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time. 
        @Volatile
        private var INSTANCE: UserRoomDatabase? = null

        fun getDatabase(context: Context): UserRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        UserRoomDatabase::class.java,
                        DBConstants.DB_NAME
                    ).build()
                INSTANCE = instance
                return instance
            }
        }
   }
}