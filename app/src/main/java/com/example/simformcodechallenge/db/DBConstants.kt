package com.example.codechallengeweatherapi.DB

object DBConstants {
    const val DB_NAME = "user_database"
    const val TABLE_USERS = "users_table"
}