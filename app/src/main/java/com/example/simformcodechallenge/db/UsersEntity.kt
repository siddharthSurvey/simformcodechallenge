package com.example.simformcodechallenge.db

import android.provider.SyncStateContract
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.codechallengeweatherapi.DB.DBConstants

@Entity(tableName = DBConstants.TABLE_USERS)
data class UsersEntity(
    @PrimaryKey(autoGenerate = true)
    val userID: Int = 0,
    val age: String?,
    val email: String?,
    val gender: String?,
    val location: String?,
    val name: String?,
    val phone: String?,
    val picture: String?
){
    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String?) {
            if (!url.isNullOrEmpty()) {
                Glide.with(view.context)
                    .load(url)
                    .circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .into(view);
            }
        }
    }
}
