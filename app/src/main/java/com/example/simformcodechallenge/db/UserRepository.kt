package com.example.codechallengeweatherapi.DB

import androidx.lifecycle.LiveData
import com.example.simformcodechallenge.db.UsersEntity
import com.example.simformcodechallenge.model.Users

class UserRepository(private val userDao: UserDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allUsers: LiveData<List<UsersEntity>> = userDao.getAllUsers()

    suspend fun insert(user: UsersEntity) {
        userDao.insertAll(user)
    }

    suspend fun deleteAll() {
        userDao.deleteAll()
    }
}