package com.example.simformcodechallenge.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.simformcodechallenge.R
import com.example.simformcodechallenge.databinding.RowUserListBinding
import com.example.simformcodechallenge.db.UsersEntity
import kotlinx.android.synthetic.main.row_user_list.view.*

class UserListAdapter(private val context: Context, private val userList: ArrayList<UsersEntity>) :
        RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListAdapter.ViewHolder {
        val binding: RowUserListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_user_list, parent, false
        )

        return ViewHolder(binding)
//        val inflater = LayoutInflater.from(parent.context)
//        val view = inflater.inflate(R.layout.row_user_list, parent, false)
//        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var user = userList[position]
        holder.bind(user)

        /*val userFullName = user.name
        holder.txtUserName.text = userFullName

        Glide.with(context)
            .load(user.picture)
            .circleCrop()
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(holder.imgProfilePic);

        holder.txtGender.text = user.gender
        holder.txtAge.text = user.age
        holder.txtLocation.text = user.location
        holder.txtEmail.text = user.email
        holder.txtPhone.text = user.phone*/

    }

    override fun getItemCount(): Int {
        return userList.size
    }

    open class ViewHolder(itemRowBinding: RowUserListBinding) : RecyclerView.ViewHolder(itemRowBinding.root) {
        var itemRowBinding: RowUserListBinding

        fun bind(obj: Any?) {
            itemRowBinding.setVariable(BR.model, obj)
            itemRowBinding.executePendingBindings()
        }

        init {
            this.itemRowBinding = itemRowBinding
        }
    }


    /*inner class ViewHolder(rowUserListBinding: RowUserListBinding) : RecyclerView.ViewHolder(rowUserListBinding.root)
    {
        var rowUserListBinding: RowUserListBinding = rowUserListBinding

        var txtUserName: TextView = itemView.txtUserName
        var imgProfilePic: ImageView = itemView.imgProfilePic
        var txtGender: TextView = itemView.txtGender
        var txtAge: TextView = itemView.txtAge
        var txtLocation: TextView = itemView.txtLocation
        var txtEmail: TextView = itemView.txtEmail
        var txtPhone: TextView = itemView.txtPhone
    }*/

}